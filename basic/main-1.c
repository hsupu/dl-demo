#include <dlfcn.h>
#include <stdio.h>

int main(int argc, char *argv[]) {
    void *handle = dlopen("./liba.so", RTLD_LAZY | RTLD_LOCAL | RTLD_DEEPBIND);
    
    int *pvar = (int *) dlsym(handle, "g_var");
    fprintf(stdout, "g_var=%d\n", *pvar);
    
    int (*pf)(int, int) = (int(*)(int, int)) dlsym(handle, "f_bop");
    fprintf(stdout, "pf(1,2)=%d\n", pf(1, 2));
    
    dlclose(handle);
    return 0;
}
