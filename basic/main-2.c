#define _GNU_SOURCE
#include <dlfcn.h>
#include <stdio.h>

typedef int (*pf_bop)(int, int);

int __attribute__((visibility("default"))) f_bop(int a, int b) {
    return a + b;
}

int main(int argc, char *argv[]) {
    void *handle = dlopen("./liba.so", RTLD_NOW | RTLD_GLOBAL | RTLD_DEEPBIND);
    if (!handle) {
        fprintf(stderr, "lib lost\n");
        return 1;
    }
    
    int *pvar = (int *) dlsym(handle, "g_var");
    if (pvar) {
        fprintf(stdout, "g_var=%d\n", *pvar);
    } else {
        fprintf(stderr, "g_var lost\n");
    }

    pf_bop pf0 = (pf_bop) dlsym(RTLD_DEFAULT, "f_bop");
    pf_bop pf1 = (pf_bop) dlsym(RTLD_NEXT, "f_bop");
    pf_bop pf2 = (pf_bop) dlsym(handle, "f_bop");

    if (pf0)
        fprintf(stdout, "pf0(1,2)=%d\n", pf0(1, 2));
    if (pf1)
        fprintf(stdout, "pf1(1,2)=%d\n", pf1(1, 2));
    if (pf2)
        fprintf(stdout, "pf2(1,2)=%d\n", pf2(1, 2));
    
    dlclose(handle);
    return 0;
}
