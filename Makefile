MAKE=make

SUBDIRS= \
	basic

.PHONY: $(SUBDIRS)
$(SUBDIRS):
	$(MAKE) -C $@
